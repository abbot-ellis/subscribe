from django.apps import AppConfig


class AelliscribeConfig(AppConfig):
    name = 'aelliscribe'
