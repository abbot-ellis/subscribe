import aellis.db.uuid as uuid

from django.db import models

# Time offset used by Python uuid library
_TIME_OFFSET = 0x1b21dd213814000

class Subscriber(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.get_uuid, editable=False
    )

    @property
    def creation_time(self):
        (self.id.int >> 68) - _TIME_OFFSET




